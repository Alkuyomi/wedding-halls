package com.weddinghalls.weddinghalls


import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_book.*
import org.jetbrains.anko.longToast

class Book : AppCompatActivity() {
    lateinit var mDatabaseReference: DatabaseReference
    private var mProgressBar: ProgressDialog? = null
    val mAuth = FirebaseAuth.getInstance()
    var mkey = ""
    var name = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book)

        mDatabaseReference = FirebaseDatabase.getInstance().getReference("reservation")
        mProgressBar = ProgressDialog(this)

        mkey = intent.getStringExtra("key")
        name = intent.getStringExtra("hall")

        subBtn.setOnClickListener {
            startActivity(Intent(this , MainActivity :: class.java))
            sendingReport(mDatabaseReference)


        }
    }

    fun sendingReport(firebaseData: DatabaseReference) {
        var user = mAuth.currentUser
        var uid = user!!.uid.toString()
       if(nameText.text.toString().length != 0 && mobileText.text.toString().length != 0 && dateText.text.toString().length != 0 ){
           val key = firebaseData.child("reservation").push().key
           firebaseData.child(key).child("name").setValue(nameText.text.toString())
           firebaseData.child(key).child("mobile" ).setValue(mobileText.text.toString())
           firebaseData.child(key).child("date").setValue(dateText.text.toString())
           firebaseData.child(key).child("hall").setValue(name)
           firebaseData.child(key).child("uid").setValue(uid)
           firebaseData.child(key).child("key").setValue(mkey)
           longToast("Hall Booked")
       }else{
           longToast("Please Enter your information")
       }



    }



}
