package com.weddinghalls.weddinghalls

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.actions.ReserveIntents
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val mAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        logoutBtn.setOnClickListener {
            mAuth!!.signOut()
            startActivity(Intent(this , Login :: class.java))
            finish()
        }

        disBtn.setOnClickListener {
            startActivity(Intent(this , DisplayHalls :: class.java))
            finish()
        }

        resBtn.setOnClickListener {
            startActivity(Intent(this , Reservations :: class.java))
            finish()
        }
    }
}
