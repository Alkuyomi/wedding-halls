package com.weddinghalls.weddinghalls

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_display_halls.*
import java.util.ArrayList

class HallsListAdapter (context: Context, list: ArrayList<Hall>): BaseAdapter() {

    var locationList: ArrayList<Hall>? = null
    var inflater: LayoutInflater? = null
    var context: Context? = null

    init {
        this.locationList = list
        this.inflater = LayoutInflater.from(context)
        this.context = context

    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {

        var view: View? = null
        view = inflater?.inflate(R.layout.hall_item , p2 , false)

        var hall: Hall

        hall = getItem(p0)

        var title_text: TextView = view?.findViewById(R.id.title_text) as TextView
        var size_text: TextView = view?.findViewById(R.id.size_text) as TextView
        var kitchen_text: TextView = view?.findViewById(R.id.kitchen_text) as TextView
        var toilets_text: TextView = view?.findViewById(R.id.toilets_text) as TextView
        var item_image: ImageView = view?.findViewById(R.id.item_image) as ImageView

        var bookBtn: TextView = view?.findViewById(R.id.book_btn) as TextView


        bookBtn.setOnClickListener {
            var intent: Intent = Intent(context , Book :: class.java)
            intent.putExtra("key" , hall.key.toString() )
            intent.putExtra("hall" , hall.title.toString() )
            context!!.startActivity(intent)        }


        Picasso.get().load(hall.image).into(item_image)

        size_text.setText("Size : "+hall.size)
        kitchen_text.setText("Kitchen : "+hall.kitchen)
        toilets_text.setText("Toilets : "+hall.toilets)
        title_text.setText(hall.title)



        return view!!
    }

    override fun getItem(p0: Int): Hall {
        return locationList?.get(p0)!!
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return locationList!!.size
    }

}