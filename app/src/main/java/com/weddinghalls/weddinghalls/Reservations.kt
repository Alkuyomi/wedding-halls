package com.weddinghalls.weddinghalls



import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_reservations.*
import org.jetbrains.anko.longToast

class Reservations : AppCompatActivity() {

    lateinit var mDatabaseReference: DatabaseReference
    private var mProgressBar: ProgressDialog? = null
    val mAuth = FirebaseAuth.getInstance()
    var listItems: ArrayList<String>? = null
    var data: ArrayList<Res>? = null
    lateinit var uid: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reservations)



        mProgressBar = ProgressDialog(this)




        listItems = ArrayList()
        data = ArrayList()

        var user = mAuth.currentUser
        uid = user!!.uid.toString()

        mDatabaseReference = FirebaseDatabase.getInstance().getReference("reservation")

        val userListener = object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                showRequests(dataSnapshot)
            }



            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("ANY", "loadPost:onCancelled", databaseError.toException())

            }
        }
        mDatabaseReference.addValueEventListener(userListener)




    }


    fun showRequests(dataSnapshot: DataSnapshot){
        listItems!!.clear()
        val children = dataSnapshot!!.children
        children.forEach {
            if( it.child("uid").getValue().toString().equals(uid+"")){
                var item: String = it.child("hall").getValue().toString()
                listItems!!.add(item)
                data!!.add(Res(it.child("hall").getValue().toString() ,it.child("mobile").getValue().toString() ,it.child("date").getValue().toString() , it.key))
            }

        }
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems)
        reqList.adapter = adapter
        reqList.setOnItemClickListener { adapterView, view, i, l ->
            var intent: Intent = Intent(this , EdiRes :: class.java)

            intent.putExtra("key" , data!!.get(i).key.toString() )
            intent.putExtra("name" , data!!.get(i).name.toString() )
            intent.putExtra("mobile" , data!!.get(i).mobile.toString() )
            intent.putExtra("date" , data!!.get(i).date.toString() )
           startActivity(intent)
        }
    }
}
