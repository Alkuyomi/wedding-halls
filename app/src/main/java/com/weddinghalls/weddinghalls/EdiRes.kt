package com.weddinghalls.weddinghalls


import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_edit_hall.*
import org.jetbrains.anko.longToast

class EdiRes : AppCompatActivity() {
    lateinit var mDatabaseReference: DatabaseReference
    private var mProgressBar: ProgressDialog? = null
    val mAuth = FirebaseAuth.getInstance()
    var mkey = ""
    var name = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_hall)

        mDatabaseReference = FirebaseDatabase.getInstance().getReference("reservation")
        mProgressBar = ProgressDialog(this)

        mkey = intent.getStringExtra("key")
        name = intent.getStringExtra("name")
        var mobile = intent.getStringExtra("mobile")
        var date = intent.getStringExtra("date")

        nameText.setText(name)
        dateText.setText(date)
        mobileText.setText(mobile)


        subBtn.setOnClickListener {
            startActivity(Intent(this , MainActivity :: class.java))
            editing(mDatabaseReference)


        }

        deleteBtn.setOnClickListener {
            startActivity(Intent(this , MainActivity :: class.java))
            delete(mDatabaseReference)


        }
    }

    fun editing(firebaseData: DatabaseReference) {
        var user = mAuth.currentUser
        var uid = user!!.uid.toString()
        if(nameText.text.toString().length != 0 && mobileText.text.toString().length != 0 && dateText.text.toString().length != 0 ){
           firebaseData.child(mkey).updateChildren(mapOf(

                "hall" to nameText.text.toString(),
                "mobile" to mobileText.text.toString(),
                "date" to dateText.text.toString()

            ))


            longToast("Reservation update ")
            startActivity(Intent(this , MainActivity :: class.java))

        }else{
            longToast("Please Enter your information")
        }



    }

    fun delete(firebaseData: DatabaseReference) {
        var user = mAuth.currentUser
        var uid = user!!.uid.toString()
            firebaseData.child(mkey).removeValue()


            longToast("Reservation deleted ")
            startActivity(Intent(this , MainActivity :: class.java))




    }



}
