package com.weddinghalls.weddinghalls

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.longToast

class Register : AppCompatActivity() {


    val  mAuth = FirebaseAuth.getInstance()
    private var mProgressBar: ProgressDialog? = null

    lateinit var mDatabaseReference: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)


        mProgressBar = ProgressDialog(this)

        mDatabaseReference = FirebaseDatabase.getInstance().getReference("Users")

        regBtn.setOnClickListener {
                view -> reg()
        }


        lBtn.setOnClickListener {
            startActivity(Intent(this , Login :: class.java))
        }
    }


    private fun reg(){
        var name = nameText.text.toString()
        var pass = passText.text.toString()
        var email = emailText.text.toString()



        if(!name.isEmpty() && !email.isEmpty() && !pass.isEmpty() ){

            if(pass.length >= 6){
                mProgressBar!!.setMessage("Registering User...")
                mProgressBar!!.show()

                mAuth!!.createUserWithEmailAndPassword(email , pass).addOnCompleteListener {
                    if (it.isSuccessful){

                        var user = mAuth!!.currentUser
                        var uid = user!!.uid.toString()
                        mDatabaseReference.child(uid).child("name").setValue(name)
                        mDatabaseReference.child(uid).child("email").setValue(email)

                        mProgressBar!!.hide()
                        startActivity(Intent(this , MainActivity :: class.java))
                        longToast("user registered")
                    }else{
                        Log.w("FIREBASE", "createUserWithEmail:failure", it.exception)
                        longToast(it.exception!!.message + "")
                        mProgressBar!!.hide()
                    }
                }

            }else{
                longToast("the password must be at least 6 characters long")
            }
        }else{
            longToast("Please Enter your info")
        }


    }


}
