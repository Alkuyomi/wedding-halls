package com.weddinghalls.weddinghalls

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_display_halls.*

class DisplayHalls : AppCompatActivity() {
    lateinit var mDatabaseReference: DatabaseReference
    private var mProgressBar: ProgressDialog? = null
    val mAuth = FirebaseAuth.getInstance()
    var listItems: ArrayList<Hall>? = null
    lateinit var uid: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_halls)




        mProgressBar = ProgressDialog(this)




        listItems = ArrayList()

        var user = mAuth.currentUser
        uid = user!!.uid.toString()

        mDatabaseReference = FirebaseDatabase.getInstance().getReference("halls")

        val userListener = object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                showRequests(dataSnapshot)
            }



            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("ANY", "loadPost:onCancelled", databaseError.toException())

            }
        }
        mDatabaseReference.addValueEventListener(userListener)




    }

    fun showRequests(dataSnapshot: DataSnapshot){
        listItems!!.clear()
        val children = dataSnapshot!!.children
        children.forEach {
                listItems!!.add(Hall(it.child("name").getValue().toString() ,
                                     it.child("size").getValue().toString() ,
                                     it.child("kitchen").getValue().toString() ,
                                      it.child("toilets").getValue().toString() ,
                                      it.child("image").getValue().toString(),
                it.key
                ))

//it.child("report").getValue().toString()
        }
        val adapter = HallsListAdapter(this, listItems!!)
        hallsList.divider = null
        hallsList.adapter = adapter


    }
}
