package com.weddinghalls.weddinghalls


import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.longToast

class Login : AppCompatActivity() {

    val mAuth = FirebaseAuth.getInstance()
    private var mProgressBar: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        var user = mAuth.currentUser

        if(user != null){
            finish()
            startActivity(Intent(this , MainActivity :: class.java))
        }else{

        }

        mProgressBar = ProgressDialog(this)

        logBtn.setOnClickListener {
                view -> login()
        }

        regBtn.setOnClickListener {
            startActivity(Intent(this , Register :: class.java))
        }


    }


    private fun login(){
        var email = emailText.text.toString()
        var pass = passwordText.text.toString()

        if(!email.isEmpty() && !pass.isEmpty()){
            mProgressBar!!.setMessage("Logging in ...")
            mProgressBar!!.show()
            mAuth!!.signInWithEmailAndPassword(email , pass).addOnCompleteListener {
                mProgressBar!!.hide()
                if (it.isSuccessful){
                    startActivity(Intent(this , MainActivity :: class.java))
                    longToast("logged in")
                }else{
                    longToast("Wrong email or password")
                }
            }

        }else{
            longToast("Please enter the email and password")
        }
    }
}
